## Title
Map Your Digital Photos on a Digital Travel Map!

## Description
This Juypter Notebook uses the ‘Image’ package to extract out the geocoordinates of your photos. The notebook will then use the ‘folium’ package to create markers on a map to visually show where you took the photos. This is a great way to show where you have been around the world.

## Visuals
https://youtu.be/gkEjV7m4LVc


## Acknowledgment
Some examples were taken from <https://medium.com/spatial-data-science/how-to-extract-gps-coordinates-from-images-in-python-e66e542af354>

## License
Use as you see fit.
